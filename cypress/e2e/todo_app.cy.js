describe('template spec', () => {
  it('add a todo', () => {
    cy.visit("http://localhost:3000")
    cy.get('button').click()
    cy.get('#title').type("Test")
    cy.get('button').click()
    cy.get('ul').contains("Test")
  }),
  it('count todo', () => {
    cy.visit("http://localhost:3000")
    cy.get('button').click()
    cy.get('#title').type("Test")
    cy.get('button').click()
    cy.get('#container > :nth-child(4)').contains("Total Todos: 1")
  }),
  it('count selected todo', () => {
    cy.visit("http://localhost:3000")
    cy.get('button').click()
    cy.get('#title').type("Test")
    cy.get('button').click()
    cy.get("input[type='checkbox']").check()
    cy.get('#container > :nth-child(5)').contains("Selected Todos: 1")
  })
})